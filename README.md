# tripleten-task-duarte



## Conclusions about the article

TypeScript: For me, TypeScript should ne a mandatory stack when creating a new project, it offers a lot of benefits we tend to forget because the most of the developers prefer to be comfy when it comes to write new code.

TypeScript makes you to be a better debeloper because the core of a Software Engineer is the creativity, the way you approach a problem and solve it, TypeScript makes you to use the specific types to be used and that way, you always have in mind the data structures you are manipulating, the outpur you are expecting, the inputs you are providing.

Not only that, TypeScript makes you full accountable of the code you have written, I mean, if you see a code that has "any" in a lot of places, or you need to cast a lot of times "unknown" you can identify the engineers that needs some mentoring and start debelop them and make them better, the goal is not only to writte quality code, at least not for me, as a side role you can be there for others who needs help, if you can lend a hand, always do it !, 

Algorithms: I agree to test the algorithm knowledge we have, but not in the way to know if that person knows it perfectly, I like to see beyond that or at least, the thing I value the most, the way the engineer thinks, the way he can be creative to solve a problem, the way he can start the thinking process, that tells you a lot, honestly I don't care if anyone knows 100 algorithms, probably it won't use them ever.  

Unit Testing: For me this is very important, unfortunately not much engineers do this, I like to see this topic as something mandatory, because is like a blueprint of the final resuilt you are expecting, you can think of it like if you are going to build a building, is exactly the same, the unit test allows you to know what are you building, and help others to understand the final resulst we can all expect.


## Brief about TypeScript: Why you should start using it?

This article won't try to make you use TypeScript to stand out among another candidates in a developer position, this article is intended to open your mind so you can understand the benefits of use TypeScript and be better when you are writtin code and after all of this, you will be able to identify why TypeScript is the way ! 

While using TypeScript, you are becoming  a better developer, let's talk about Static Type Checker;

Consider this code:

JavaScript coerces its operands, this will cause pretty weird errors

```
if ("" == 0) {
  // It is! But why??
}
if (1 < x < 3) {
  // True for *any* value of x!
}
```

JavaScript also allows to access things that does not exist:

```
const obj = { width: 10, height: 15 };
// Why is this NaN? Spelling is hard!
const area = obj.width * obj.heigth;
```

Now imagine you are working in a very large project and start to see errors like this, where do you start to debug? You are right, it's a mess ! Not with TypeScript.

TypeScript checks a program for errors before exectuion, and does so based on the kinds of values, making it a static type checker.

This what will happen with TypeScript:

```
const obj = { width: 10, height: 15 };
// Why is this NaN? Spelling is hard!
const area = obj.width * obj.heigth;
Property 'heigth' does not exist on type '{ width: number; height: number; }'. Did you mean 'height'?
```

### A Typed Superset of JavaScript

 TypeScript is a typed superset, meaning that it adds rules about how different kinds of values can be used. 
 
 ```
 console.log(4 / []);
 ```

This syntactically-legal program logs Infinity. TypeScript, however, considers division of number by an array to be a nonsensical operation, and will issue an error like this:
 
 ```
The right-hand side of an arithmetic operation must be of type 'any', 'number', 'bigint' or an enum type.
 ```
 
 So, at this point, I'm sure you are able to see one of the many benefits TypeScript has to offer, naturally, you can't learn TypeScript without knowing JavaScript, this being said, when you are learning JavaSript, keep in mind, if something does not make sense in JavaScript, don't do it ! TypeScipt will tell you later (If you turn your code into TypeScript code)